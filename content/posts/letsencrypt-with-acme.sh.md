+++
author = "Simon"
title = 'LetsEncrypt with acme.sh and Cloudflare DNS'
date = "2024-06-29"
description = "Using acme.sh to automate LetsEncrypt certificates with Cloudflare DNS"
+++

## Introduction

[Let's Encrypt](https://letsencrypt.org/) officially launched in April 2016 having already issued over 1.7 million certificates after a period of development, testing, and beta phases stretching back to 2012.

Their free and [fully automated certificates](https://letsencrypt.org/docs/client-options/) have shaken up the internet and greatly improved the security of most websites allowing people to protect themselves and their visitors.

At the time of writing there are two validation methods to validate ownership of the domain(s) when issuing certificates, HTTP and DNS based. DNS having the added benefit of allowing wild card certificates!

This post will be focusing on issuing a wild card certificate with the [acme.sh](https://github.com/acmesh-official/acme.sh) client. There are many clients out there but I like this one because it's pure shell script (with some common external dependencies such as cURL) so it's light weight and will run pretty much anywhere as a standard user.

## Installing acme.sh

  1. [Install acme.sh](https://github.com/acmesh-official/acme.sh).
     - There are a few methods and they may change over time so I have not replicated them here.
     - Personally I tend to clone the git repository and run the installer that way as I'm generally against the `curl | sh` pattern.
  1. [Set the default issuer server](https://github.com/acmesh-official/acme.sh/wiki/Server) to `letsencrypt_test` or if you're feeling confident `letsencrypt`.
      - This is a personal choice but this article _is_ about Let's Encrypt ;). At the time of writing `acme.sh` defaults to ZeroSSL.com and there are other supported CAs you can choose from.
      - If this is your first time doing this I would highly recommend using the test server for the CA you pick as (certainly LetsEncrypt) has rate limits on their live servers and you could end up being blocked for a day or more if you hit a limit. Test servers are a lot more lenient so you can test and get things right before switching to live and reissuing.

        ```shell
        acme.sh --set-default-ca --server letsencrypt_test
        ```

        All being well you should see something like this:

        ```shell
        [Thu Jun 29 14:44:18 BST 2024] Changed default CA to: https://acme-staging-v02.api.letsencrypt.org/directory
        ```

## Setting up Cloudflare

As we mentioned earlier we are going to issue a wild card certificate and that means we need to do DNS based validation. acme.sh supports many DNS provider APIs, so many   the [list](https://github.com/acmesh-official/acme.sh/wiki/dnsapi) spread over [two](https://github.com/acmesh-official/acme.sh/wiki/dnsapi2) wiki pages!

If you don't use Cloudflare then I would advise consulting the acme.sh wiki to see how to setup for your provider.

1. Login to the [Cloudflare dashboard](https://dash.cloudflare.com/) and head to your Profile, then [API Tokens](https://dash.cloudflare.com/profile/api-tokens).
1. Click `Create Token`.
1. Click `Use template` next to `Edit zone DNS`.
1. Fill in your details:
    - `Token name` defaults to `Edit zone DNS` but I would highly recommend changing this to something relevant to help you keep track of your API tokens.
    - `Permissions` - This should already be set to `Zone/DNS/Edit`.
    - `Zone Resources` - You can choose to restrict this API token to certain DNS zones or allow access to everything, my personal choice is to restrict the to the domain(s) I know I want to issue from this particular server/device.
    - `Client IP Address Filtering` - Restrict this API token to certain IP addresses, again my personal choice is restrict things where possible. If you are using the token on something with a fixed IP I would recommend setting this.
    - `TTL` - An optional expiry date for the token. If you do set this you may want to setup a reminder somewhere to rotate the token.
    - Hit `Continue to summary`.
1. Check everything looks good and click `Create Token`.
1. Make a note of the token somewhere secure, or leave this tab open for now until we enter it into acme.sh. Cloudflare also provide a cURL based one liner that you can copy and paste to test the token. Remember if you restricted the token to certain IP addresses you will need to test from those!

## Issuing a certificate

Almost there!

Take the API token we just created and add it to your shell environment:

```shell
export CF_Token="<CF_API_KEY>"
```

If you are using a different DNS provider this step will be different, the acme.sh wiki should have you covered.

acme.sh will save this in it's configuration file when you first issue a certificate so you don't need to worry about persistence.

Now we can request and get our certificate, enter `example.com`! We're going to issue one certificate with two domains in the [Subject Alternative Name](https://en.wikipedia.org/wiki/Subject_Alternative_Name) (SAN) field. You can add more if you want but remember that all of the domains you choose need to be on your Cloudflare account and your API token needs to include them on its allow list.

```shell
acme.sh --issue --dns dns_cf --ocsp-must-staple --keylength 4096 -d example.com -d '*.example.com'
```

There's a lot going on here so lets break it down:

- `--issue` - we want to `issue` a new certificate.
- `--dns dns_cf` - we want to use a `dns` plugin, specifically the `dns_cf` plugin so we can talk to Cloudflare. If you are using a different DNS provider then check what you need to use here instead.
- `--oscp-must-stable` - enable [OCSP stapling](https://en.wikipedia.org/wiki/OCSP_stapling) this is optional and will likely need additional configuration in the application(s) that you use the certificate (e.g. [NGINX](https://nginx.org/en/docs/http/ngx_http_ssl_module.html#ssl_stapling)) with however I highly recommend using it if you know you can.
  - If you later find you didn't want this you can rerun the command without this flag and add `--force` to make acme.sh request a new certificate without this flag.
- `--keylength 4096` - generate a 4096 bit RSA key for this certificate. At the moment 2048 is generally considered secure (and faster) so this is a personal choice. In a minute we will also generate a ECC based key which is more secure for the same key size and faster.
- `-d example.com` - the domain(s) we want on this certificate, you can specify multiple `-d` options but make sure to quote any wild cards to avoid shell expansion.

If you want to use ECC instead, or as well if your software supports both, then you can change the `--keylength` argument to `--keylength ec-384`

```shell
acme.sh --issue --dns dns_cf --ocsp-must-staple --keylength ec-384 -d example.com -d '*.example.com'
```

All being well you should see some output similar to:

```shell
[Thu Jun 29 14:50:43 BST 2024] Cert success.
```

along with some details on where everything has been stored.

You should not use these paths in your application(s) because they may change in the future but acme.sh has another command we can run to install these certificates exactly where we want them.

## Installing the certificate and key

Different distributions/people have different ideas about where they keep keys and certificates, which is fine. Personally I'm using the following at the moment:

- `/etc/tls/keys` for keys.
- `/etc/tls/certs` for certificates.

Install the certificate:

```shell
acme.sh --install-cert --domain example.com --cert-file /etc/tls/certs/example.com.pem --key-file /etc/tls/keys/example.com.pem --fullchain-file /etc/tls/certs/example.com.fullchain.pem --ca-file /etc/tls/certs/example.com.cabundle.pem --reloadcmd "systemctl reload nginx.service"
```

Lots of arguments here again so let's break them down:

- `--install-cert` - we want to install our certificate.
- `--domain example.com` - the primary domain of the certificate we want to install (this is the first one you specified with `-d` earlier).
- `--cert-file /etc/tls/certs/example.com.pem`* - path to save the certificate file to.
- `--key-file /etc/tls/keys/example.com.pem`* - path to save the key file to.
- `--fullchain-file /etc/tls/certs/example.com.fullchain.pem`* - path to save the full chain to.
- `--ca-file /etc/tls/certs/example.com.cabundle.pem`* - path to save the CA bundle to.
- `--reloadcmd "systemctl reload nginx.service"`* - a command to run now and every time the certificate is renewed in the future.

`*` denotes optional arguments.

If you issued an ECC certificate you can add `--ecc`, if you are using both make sure to change all of the file paths to avoid overwriting the first ones.

You can update any of these in the future by running the command again.

Whilst the `reloadcmd` argument is saved base64 encoded so you can stack more than one reload/restart command there I would personally advise specifying a script file so you can easily add/remove things and also do anything else you might need to at the same time. As an example I have a Java based service running in a docker container so when the certificate renews I have to rebuild the keystore file which is a bit complex for a one liner.

## Notifications

acme.sh has support for [notifications](https://github.com/acmesh-official/acme.sh/wiki/notify). These are sent when the cron task runs depending on the level you set.

I won't go in to the details here as there's many options available and the wiki page covers how to set them all up.

I personally use the Discord one with the default level (2) and mode(0). This means I get a notification when something happens, be it a successful renewal or a failure. Although I am considering switching to level 1 as I only really care about failures.

## Closing

Congratulations, you now have a free 90 day SSL certificate for your website/application!

Only 90 days? but paid ones are 1 year I hear you cry!

Indeed, but you just automated your renewal so you longer need to worry about remembering to renew/setup your certificates every year. Especially if you setup notifications you will know if this automation ever fails so you can take action.

acme.sh defaults to renewing after 60 days so you get 30 days wiggle room to solve any problems that do crop up. You can change this with `acme.sh --days NN` if you want.
